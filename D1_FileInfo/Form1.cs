﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D1_FileInfo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            outputLabel.Text = "";
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            string filename = filenameText.Text;
            try
            {
                if (File.Exists(filename))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("File exists");
                    sb.AppendLine("File was created " + File.GetCreationTime(filename));
                    sb.AppendLine("File was last accessed " + File.GetLastAccessTime(filename));
                    sb.AppendLine("File was last written to " + File.GetLastWriteTime(filename));

                    outputLabel.Text = sb.ToString();
                }
                else
                {
                    outputLabel.Text = "File does not exist";
                }
            }
            catch (IOException ex)
            {
                outputLabel.Text = ex.ToString();
            }
        }
    }
}
