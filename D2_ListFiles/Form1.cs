﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D2_ListFiles
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            outputLabel.Text = "";
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            string dir = dirnameText.Text;
            try
            {
                if (Directory.Exists(dir))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.AppendLine("Directory exists");
                    sb.AppendLine("Directory was created " + Directory.GetCreationTime(dir));
                    sb.AppendLine("Directory was last accessed " + Directory.GetLastAccessTime(dir));
                    sb.AppendLine("Directory was last written to " + Directory.GetLastWriteTime(dir));
                    sb.AppendLine();

                    string[] fileList = Directory.GetFiles(dir);
                    foreach (string file in fileList)
                    {
                        sb.AppendLine("    " + file);
                    }

                    outputLabel.Text = sb.ToString();
                }
                else
                {
                    outputLabel.Text = "Directory does not exist";
                }
            }
            catch (IOException ex)
            {
                outputLabel.Text = ex.ToString();
            }
        }
    }
}
