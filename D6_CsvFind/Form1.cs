﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D6_CsvFind
{
    public partial class Form1 : Form
    {
        private const string FILENAME = "EmployeeData.txt";

        public Form1()
        {
            InitializeComponent();
            outputLabel.Text = "";
        }

        private void findButton_Click(object sender, EventArgs e)
        {
            string filename = FILENAME;
            try
            {
                double minSalary = double.Parse(salaryText.Text);

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(
                    "{0,-5}{1,-12}{2,8}",
                    "Num", "Name", "Salary"
                );
                sb.AppendLine();

                using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            string[] fields = line.Split(',');

                            Employee emp = new Employee();
                            emp.Id = int.Parse(fields[0]);
                            emp.Name = fields[1];
                            emp.Salary = double.Parse(fields[2]);

                            if (emp.Salary >= minSalary)
                            {
                                sb.AppendFormat(
                                    "{0,-5}{1,-12}{2,8:C}",
                                    emp.Id, emp.Name, emp.Salary
                                );
                                sb.AppendLine();
                            }
                        }
                    }
                }

                outputLabel.Text = sb.ToString();
            }
            catch (Exception ex)
            {
                outputLabel.Text = ex.ToString();
            }
        }
    }
}
