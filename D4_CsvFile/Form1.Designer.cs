﻿namespace D4_CsvFile
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.empNumLabel = new System.Windows.Forms.Label();
            this.lastNameLabel = new System.Windows.Forms.Label();
            this.salaryLabel = new System.Windows.Forms.Label();
            this.outputLabel = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.empNumText = new System.Windows.Forms.TextBox();
            this.lastNameText = new System.Windows.Forms.TextBox();
            this.salaryText = new System.Windows.Forms.TextBox();
            this.doneButton = new System.Windows.Forms.Button();
            this.openButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // empNumLabel
            // 
            this.empNumLabel.AutoSize = true;
            this.empNumLabel.Location = new System.Drawing.Point(12, 9);
            this.empNumLabel.Name = "empNumLabel";
            this.empNumLabel.Size = new System.Drawing.Size(63, 13);
            this.empNumLabel.TabIndex = 0;
            this.empNumLabel.Text = "Employee #";
            // 
            // lastNameLabel
            // 
            this.lastNameLabel.AutoSize = true;
            this.lastNameLabel.Location = new System.Drawing.Point(132, 9);
            this.lastNameLabel.Name = "lastNameLabel";
            this.lastNameLabel.Size = new System.Drawing.Size(58, 13);
            this.lastNameLabel.TabIndex = 2;
            this.lastNameLabel.Text = "Last Name";
            // 
            // salaryLabel
            // 
            this.salaryLabel.AutoSize = true;
            this.salaryLabel.Location = new System.Drawing.Point(251, 9);
            this.salaryLabel.Name = "salaryLabel";
            this.salaryLabel.Size = new System.Drawing.Size(36, 13);
            this.salaryLabel.TabIndex = 4;
            this.salaryLabel.Text = "Salary";
            // 
            // outputLabel
            // 
            this.outputLabel.AutoSize = true;
            this.outputLabel.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputLabel.Location = new System.Drawing.Point(12, 98);
            this.outputLabel.Name = "outputLabel";
            this.outputLabel.Size = new System.Drawing.Size(68, 18);
            this.outputLabel.TabIndex = 8;
            this.outputLabel.Text = "output";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(373, 24);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(75, 23);
            this.addButton.TabIndex = 6;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // empNumText
            // 
            this.empNumText.Location = new System.Drawing.Point(15, 26);
            this.empNumText.Name = "empNumText";
            this.empNumText.Size = new System.Drawing.Size(100, 20);
            this.empNumText.TabIndex = 1;
            // 
            // lastNameText
            // 
            this.lastNameText.Location = new System.Drawing.Point(135, 26);
            this.lastNameText.Name = "lastNameText";
            this.lastNameText.Size = new System.Drawing.Size(100, 20);
            this.lastNameText.TabIndex = 3;
            // 
            // salaryText
            // 
            this.salaryText.Location = new System.Drawing.Point(254, 26);
            this.salaryText.Name = "salaryText";
            this.salaryText.Size = new System.Drawing.Size(100, 20);
            this.salaryText.TabIndex = 5;
            // 
            // doneButton
            // 
            this.doneButton.Location = new System.Drawing.Point(373, 54);
            this.doneButton.Name = "doneButton";
            this.doneButton.Size = new System.Drawing.Size(75, 23);
            this.doneButton.TabIndex = 7;
            this.doneButton.Text = "Done";
            this.doneButton.UseVisualStyleBackColor = true;
            this.doneButton.Click += new System.EventHandler(this.doneButton_Click);
            // 
            // openButton
            // 
            this.openButton.Location = new System.Drawing.Point(331, 351);
            this.openButton.Name = "openButton";
            this.openButton.Size = new System.Drawing.Size(117, 23);
            this.openButton.TabIndex = 9;
            this.openButton.Text = "Open";
            this.openButton.UseVisualStyleBackColor = true;
            this.openButton.Click += new System.EventHandler(this.openButton_Click);
            // 
            // Form1
            // 
            this.AcceptButton = this.addButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 386);
            this.Controls.Add(this.openButton);
            this.Controls.Add(this.doneButton);
            this.Controls.Add(this.salaryText);
            this.Controls.Add(this.lastNameText);
            this.Controls.Add(this.empNumText);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.outputLabel);
            this.Controls.Add(this.salaryLabel);
            this.Controls.Add(this.lastNameLabel);
            this.Controls.Add(this.empNumLabel);
            this.Name = "Form1";
            this.Text = "CSV Writer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label empNumLabel;
        private System.Windows.Forms.Label lastNameLabel;
        private System.Windows.Forms.Label salaryLabel;
        private System.Windows.Forms.Label outputLabel;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.TextBox empNumText;
        private System.Windows.Forms.TextBox lastNameText;
        private System.Windows.Forms.TextBox salaryText;
        private System.Windows.Forms.Button doneButton;
        private System.Windows.Forms.Button openButton;
    }
}

