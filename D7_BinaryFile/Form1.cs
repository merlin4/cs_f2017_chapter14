﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D7_BinaryFile
{
    public partial class Form1 : Form
    {
        private const string FILENAME = "EmployeeData.dat";
        private bool _done;
        private FileStream _stream;
        private BinaryFormatter _formatter;

        public Form1()
        {
            InitializeComponent();
            outputLabel.Text = "";

            try
            {
                _done = false;
                _stream = new FileStream(FILENAME, FileMode.Create, FileAccess.Write);
                _formatter = new BinaryFormatter();
            }
            catch (Exception ex)
            {
                if (_stream != null)
                {
                    _stream.Close();
                }
                throw ex;
            }
        }

        private void OnDone()
        {
            if (!_done)
            {
                _stream.Close();
                _done = true;

                empNumText.Enabled = false;
                lastNameText.Enabled = false;
                salaryText.Enabled = false;

                outputLabel.Text = "DONE";
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            OnDone();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (_done) { return; }

            try
            {
                Employee emp = new Employee();
                emp.Id = int.Parse(empNumText.Text);
                emp.Name = lastNameText.Text;
                emp.Salary = double.Parse(salaryText.Text);

                _formatter.Serialize(_stream, emp);
                _stream.Flush();
                outputLabel.Text = string.Format("{0},{1},{2}", emp.Id, emp.Name, emp.Salary);

                empNumText.Focus();
                empNumText.Text = "";
                lastNameText.Text = "";
                salaryText.Text = "";
            }
            catch (Exception ex)
            {
                outputLabel.Text = ex.ToString();
            }
        }

        private void doneButton_Click(object sender, EventArgs e)
        {
            OnDone();

            string filename = FILENAME;
            try
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(
                    "{0,-5}{1,-12}{2,8}",
                    "Num", "Name", "Salary"
                );

                sb.AppendLine();
                using (var stream = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    while (stream.Position < stream.Length)
                    {
                        Employee emp = (Employee)_formatter.Deserialize(stream);
                        sb.AppendFormat(
                            "{0,-5}{1,-12}{2,8:C}",
                            emp.Id, emp.Name, emp.Salary
                        );
                        sb.AppendLine();
                    }
                }

                outputLabel.Text = sb.ToString();
            }
            catch (Exception ex)
            {
                outputLabel.Text = ex.ToString();
            }
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("notepad.exe", FILENAME);
            }
            catch (Exception ex)
            {
                outputLabel.Text = ex.ToString();
            }
        }
    }
}
