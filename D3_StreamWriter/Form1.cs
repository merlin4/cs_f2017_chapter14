﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D3_StreamWriter
{
    public partial class Form1 : Form
    {
        private const string FILENAME = "SomeText.txt";

        public Form1()
        {
            InitializeComponent();
            outputLabel.Text = "";
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            string filename = FILENAME;
            string input = inputText.Text;
            try
            {
                using (var stream = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.WriteLine(input);
                        writer.Flush();
                        writer.Close();
                    }
                }
                outputLabel.Text = "Saved " + DateTime.Now;
            }
            catch (IOException ex)
            {
                outputLabel.Text = ex.ToString();
            }
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(FILENAME);
            }
            catch (Exception ex)
            {
                outputLabel.Text = ex.ToString();
            }
        }
    }
}
