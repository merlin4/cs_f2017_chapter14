﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace D4_CsvFile
{
    public partial class Form1 : Form
    {
        private const string FILENAME = "EmployeeData.txt";
        private bool _done;
        private FileStream _stream;
        private StreamWriter _writer;
        
        public Form1()
        {
            InitializeComponent();
            outputLabel.Text = "";

            try
            {
                _done = false;
                _stream = new FileStream(FILENAME, FileMode.Create, FileAccess.Write);
                _writer = new StreamWriter(_stream);
            }
            catch (Exception ex)
            {
                if (_writer != null)
                {
                    _writer.Close();
                }
                if (_stream != null)
                {
                    _stream.Close();
                }
                throw ex;
            }
        }

        private void OnDone()
        {
            if (!_done)
            {
                _writer.Close();
                _stream.Close();
                _done = true;

                empNumText.Enabled = false;
                lastNameText.Enabled = false;
                salaryText.Enabled = false;

                outputLabel.Text = "DONE";
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            OnDone();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if (_done) { return; }

            try
            {
                Employee emp = new Employee();
                emp.Id = int.Parse(empNumText.Text);
                emp.Name = lastNameText.Text;
                emp.Salary = double.Parse(salaryText.Text);

                string line = string.Format("{0},{1},{2}", emp.Id, emp.Name, emp.Salary);
                _writer.WriteLine(line);
                _writer.Flush();
                outputLabel.Text = line;

                empNumText.Focus();
                empNumText.Text = "";
                lastNameText.Text = "";
                salaryText.Text = "";
            }
            catch (Exception ex)
            {
                outputLabel.Text = ex.ToString();
            }
        }

        private void doneButton_Click(object sender, EventArgs e)
        {
            OnDone();
        }

        private void openButton_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(FILENAME);
            }
            catch (Exception ex)
            {
                outputLabel.Text = ex.ToString();
            }
        }
    }
}
